package lu.buttel.yarnimager;

import controlP5.Button;
import controlP5.Slider;
import controlP5.ControlP5;
import lu.buttel.yarnimager.util.*;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;

import java.util.ArrayList;
import java.util.HashMap;

public class Gui extends PApplet {
	
	private PImage img, img2, img3;
	
	private int imageWidth, imageHeight,
			windowWidth, windowHeight;
	
	private int nPins = 200;
	private int minPinDistance = 10;
	private int fadeValue = 50;
	private int nStrings = 3000;
	
	private ArrayList<Pin> steps;
	
	private boolean redraw;
	private boolean triggerRedraw;
	
	public static void main(String[] args) {
		
		PApplet.main("lu.buttel.yarnimager.Gui");
	}
	
	public void settings() {
		
		windowWidth = displayWidth * 15 >> 4;
		windowHeight = displayHeight * 15 >> 4;
		imageWidth = windowWidth / 2;
		imageHeight = imageWidth;
		
		size(windowWidth, windowHeight);
		
	}
	
	public void setup() {
		
		background(255);
		
		img = loadImage("image.jpg");
		img.filter(PConstants.GRAY);
		img.resize(imageWidth, imageHeight);
		int radius = imageWidth / 2;
		for (int i = 0; i < imageWidth; i++) {
			for (int j = 0; j < imageHeight; j++) {
				if ((i - radius) * (i - radius) + (j - radius) * (j - radius) > radius * radius) {
					img.set(i, j, color(255, 1));
				}
			}
		}
		image(img, 0, 0);
		
		img2 = createImage(imageWidth, imageHeight, ARGB);
		img2.copy(img, 0, 0, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight);
		
		img3 = createImage(imageWidth, imageHeight, ARGB);
		img3.copy(img, 0, 0, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight);
		
		int scale = 4;
		img3.resize(imageWidth / scale, imageHeight / scale);
		image(img3, imageWidth - img3.width / 2, 0);
		
		ControlP5 cp5 = new ControlP5(this);
		
		Slider nPinsSlider = cp5.addSlider("nPins")
				                     .setPosition(0, windowHeight - 40)
				                     .setSize(windowWidth, 20)
				                     .setCaptionLabel("number of pins")
				                     .setRange(3, 300)
				                     .setValue(200)
				                     .setSliderMode(Slider.FIX);
		nPinsSlider.getCaptionLabel().align(CENTER, CENTER);
		
		Slider distanceSlider = cp5.addSlider("minPinDistance")
				                        .setPosition(0, windowHeight - 20)
				                        .setSize(windowWidth, 20)
				                        .setCaptionLabel("min. distance between pins")
				                        .setRange(0, 100)
				                        .setValue(10)
				                        .setSliderMode(Slider.FIX);
		distanceSlider.getCaptionLabel().align(CENTER, CENTER);
		
		
		Slider fadeSlider = cp5.addSlider("fadeValue")
				                    .setPosition(0, windowHeight - 60)
				                    .setSize(windowWidth, 20)
				                    .setCaptionLabel("fade/opacity/thickness of yarn")
				                    .setRange(0, 255)
				                    .setValue(50)
				                    .setSliderMode(Slider.FIX);
		fadeSlider.getCaptionLabel().align(CENTER, CENTER);
		
		Slider nStringSlider = cp5.addSlider("nStrings")
				                       .setPosition(0, windowHeight - 80)
				                       .setSize(windowWidth, 20)
				                       .setCaptionLabel("number of strings")
				                       .setRange(500, 7500)
				                       .setValue(750)
				                       .setSliderMode(Slider.FIX);
		nStringSlider.getCaptionLabel().align(CENTER, CENTER);
		
		Button drawButton = cp5.addButton("triggerDraw")
				                    .setPosition(imageWidth - 20, imageHeight - 40)
				                    .setSize(40, 30)
				                    .setCaptionLabel("Draw!");
		
		Button saveButton = cp5.addButton("save")
				                    .setPosition(windowWidth - 50, imageHeight - 40)
				                    .setSize(40, 30)
				                    .setCaptionLabel("Save");
		
		redraw = true;
	}
	
	public void draw() {
		
		if (redraw) {
			//TODO create methods for separate calculations/drawings
			
			img2.copy(img, 0, 0, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight);
			
			fill(255);
			noStroke();
			rect(imageWidth, 0, imageWidth, imageHeight);
			rect(0, 0, imageWidth, imageHeight);
			
			ArrayList<Pin> pins = YarnImager.calculatePinPositions(imageWidth / 2, imageHeight / 2, imageHeight / 2 - 1, nPins);
			
			fill(0);
			stroke(0);
			for (Pin pin : pins) {
				point(pin.getPosition().x + imageWidth, pin.getPosition().y);
			}
			
			
			HashMap<Pair, ArrayList<Point>> pairs = YarnImager.calculatePinPairsAndLines(pins, minPinDistance);
			
			steps = YarnImager.calculatePattern(pins, nStrings, nPins, fadeValue, new HashMap<>(pairs), new Image(imageWidth, imageHeight, img2.pixels));
			
			stroke(color(0, fadeValue));
			noFill();
			for (int i = 0; i < steps.size() - 1; i++) {
				line(steps.get(i).getPosition().x + imageWidth, steps.get(i).getPosition().y,
				     steps.get(i + 1).getPosition().x + imageWidth, steps.get(i + 1).getPosition().y);
			}
			
			image(img3, imageWidth - img3.width / 2, 0);
			image(img2, 0, 0);
			
			System.out.println("Estimated yarn length: " + YarnImager.calculateYarnLength(imageWidth, steps, 280) / 1000.0 + "m");
			redraw = false;
		}
	}
	
	@SuppressWarnings("unused")
	public void nPins(int value) {
		
		if (value != nPins) {
			nPins = value;
			triggerRedraw = true;
		}
	}
	
	@SuppressWarnings("unused")
	public void nStrings(int value) {
		
		if (value != nStrings) {
			nStrings = value;
			triggerRedraw = true;
		}
	}
	
	@SuppressWarnings("unused")
	public void minPinDistance(int value) {
		
		if (value != minPinDistance) {
			minPinDistance = value;
			triggerRedraw = true;
		}
	}
	
	@SuppressWarnings("unused")
	public void fadeValue(int value) {
		
		if (value != fadeValue) {
			fadeValue = value;
			triggerRedraw = true;
		}
	}
	
	@SuppressWarnings("unused")
	public void triggerDraw() {
		
		if (triggerRedraw) {
			redraw = true;
			triggerRedraw = false;
		}
	}
	
	@SuppressWarnings("unused")
	public void save() {
		
		StringBuilder sbTxt = new StringBuilder();
		StringBuilder sbHtml = new StringBuilder();
		
		sbHtml.append("<!DOCTYPE html> <html lang=\"en\"> <head> <meta charset=\"UTF-8\"> <title> Yarn Imager Instructio" +
				              "ns </title> <style> * {font-family:sans-serif; box-sizing: border-box;}body {margin: 0; f" +
				              "ont-size: 3vw;}div {text-align: center; color: #a9b7c6; padding-top: 1vw;}input {text-ali" +
				              "gn:center; font-size: 3vw; background: #545454; color: #a9b7c6;}.step {display: table; wi" +
				              "dth: 100vw; height: 10vh; background: #1f1f1f;}.pin {font-size: 12vw;}.left {left: 0px; w" +
				              "idth: 50%; height: 90vh; background: #545454; position: absolute;}.right {right: 0px; wid" +
				              "th: 50%; height: 90vh; background: #2b2b2b; position: absolute;} </style> <script src=\"h" +
				              "ttps://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"> </script> <script src=" +
				              "'https://code.responsivevoice.org/responsivevoice.js'> </script> <script type=\"text/java" +
				              "script\"> var steps = [");
		
		for (int i = 0; i < steps.size() - 1; i++) {
			
			Pin a = steps.get(i);
			Pin b = steps.get(i + 1);
			
			sbTxt.append("Step ")
					.append(i + 1)
					.append(": #")
					.append(a.getNumber())
					.append(" -> #")
					.append(b.getNumber());
			
			if (i == 0) {
				sbHtml.append(a.getNumber());
			}
			sbHtml.append(",\"")
					.append(b.getNumber());
			
			if (i < steps.size() - 2) {
				
				Pin c = steps.get(i + 2);
				int det = (a.getPosition().x - b.getPosition().x) * (c.getPosition().y - b.getPosition().y)
						          - (a.getPosition().y - b.getPosition().y) * (c.getPosition().x - b.getPosition().x);
				
				if (det > 0) {
					sbTxt.append(" left");
					sbHtml.append(" left\"");
				} else {
					sbTxt.append(" right");
					sbHtml.append(" right\"");
				}
				sbTxt.append("\r\n");
			}
		}
		
		sbHtml.append("\"]; var currentStep = 0; function nextStep(){if (currentStep < steps.length - 2){currentStep++; " +
				              "showStep();}} function previousStep(){if (currentStep > 0){currentStep--; showStep();}} f" +
				              "unction showStep(){$(\"#step\").val(currentStep+1); $(\"#fromPin\").text(steps[currentSte" +
				              "p]); $(\"#toPin\").text(steps[currentStep+1]); responsiveVoice.speak(steps[currentStep+1]" +
				              ".toString());} function jumpTo(){currentStep=parseInt($(\"#step\").val()) - 1; if(current" +
				              "Step < 0){currentStep = 0;} else if(currentStep > steps.length - 2){currentStep = steps.l" +
				              "ength - 2;} showStep();} function keyInput(event){switch(event.which){default: break; cas" +
				              "e 13: case 32: nextStep(); break; case 8: previousStep(); break;}} </script> </head> <bod" +
				              "y onload=\"showStep()\" onkeyup=\"keyInput(event)\"> <div class=\"step\"> Step <input typ" +
				              "e=\"tel\" id=\"step\" onchange=\"jumpTo()\"> </div> <div class=\"left\" onclick=\"previou" +
				              "sStep()\"> from <p class=\"pin\" id=\"fromPin\"> ??? </p> </div> <div class=\"right\" onc" +
				              "lick=\"nextStep()\"> to <p class=\"pin\" id=\"toPin\"> ??? </p> </dv> </body> </html>");
		
		saveBytes("out/instructions.txt", sbTxt.toString().getBytes());
		saveBytes("out/instructions.html", sbHtml.toString().getBytes());
	}
}

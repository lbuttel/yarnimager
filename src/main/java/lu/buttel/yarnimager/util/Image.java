package lu.buttel.yarnimager.util;

import javax.sound.midi.Soundbank;
import java.awt.*;
import java.util.Set;

public class Image {
	
	private int width;
	private int height;
	
	private int[] pixels;
	
	public Image(int width, int height, int[] pixels) {
		
		this.width = width;
		this.height = height;
		this.pixels = new int[pixels.length];
		System.arraycopy(pixels, 0, this.pixels, 0, pixels.length - 1);
	}
	
	public int getWidth() {
		
		return width;
	}
	
	public int getHeight() {
		
		return height;
	}
	
	public int[] getPixels() {
		
		return pixels;
	}
	
	public void setPixels(int[] pixels) {
		
		this.pixels = pixels;
	}
	
	public int get(int x, int y) {
		
		return pixels[x + y * width];
	}
	
	public void set(int x, int y, int color) {
		
		pixels[x + y * width] = color;
	}
}

package lu.buttel.yarnimager.util;

public class Pair {
	
	private final Pin a;
	private final Pin b;
	
	public Pair(Pin a, Pin b) {
		
		this.a = a;
		this.b = b;
	}
	
	public Pin getA() {
		
		return a;
	}

	public Pin getB() {
		
		return b;
	}
	
	public Pin getOther(Pin pin) {
		
		if (pin.equals(a)) {
			return b;
		} else if (pin.equals(b)) {
			return a;
		} else {
			return null;
		}
	}
	
	public boolean contains(Pin point) {
		
		return point.equals(a) || point.equals(b);
	}
	
	@Override
	public boolean equals(Object o) {
		
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		Pair other = (Pair) o;
		
//		if (!a.equals(pair.a)) return false;
//		return b.equals(pair.b);
		
		if (a.equals(other.a) && b.equals(other.b)) {
			return true;
		} else if (a.equals(other.b) && b.equals(other.a)) {
			return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		
		int result = a.hashCode();
		result = 31 * (result + b.hashCode()) ;
		return result;
	}
}

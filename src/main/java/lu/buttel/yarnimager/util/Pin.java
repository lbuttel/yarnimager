package lu.buttel.yarnimager.util;

public class Pin {
	
	private final Point position;
	private final int number;
	
	public Pin(int x, int y, int number) {
		this.position = new Point(x, y);
		this.number = number;
	}
	
	public Point getPosition() {
		return position;
	}
	
	public int getNumber() {
		return number;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		Pin pin = (Pin) o;
		
		if (position.x != pin.getPosition().x) return false;
		if (position.y != pin.getPosition().y) return false;
		return number == pin.number;
	}
	
	@Override
	public int hashCode() {
		int result = getPosition().x;
		result = 31 * result + getPosition().y;
		result = 31 * result + number;
		return result;
	}
}

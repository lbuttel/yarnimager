package lu.buttel.yarnimager.util;

import processing.core.PConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class YarnImager {
	
	public static ArrayList<Pin> calculatePinPositions(int centerX, int centerY, int radius, int nPins) {
		
		ArrayList<Pin> pins = new ArrayList<>();
		
		float angle = PConstants.PI * 2 / nPins;
		
		for (int i = 0; i < nPins; i++) {
			pins.add(new Pin((int) Math.round(centerX + radius * Math.cos(angle * i)),
			                 (int) Math.round(centerY - radius * Math.sin(angle * i)),
			                 i));
			
		}
		
		return pins;
	}
	
	public static HashMap<Pair, ArrayList<Point>> calculatePinPairsAndLines(ArrayList<Pin> pins, int minPinDistance) {
		
		HashMap<Pair, ArrayList<Point>> pairs = new HashMap<>();
		
		int nPins = pins.size();
		int nPartners = nPins - (2 * minPinDistance + 1);
		
		for (int i = 0; i < nPins; i++) {
			for (int j = 0; j < nPartners; j++) {
				
				Pair pair = new Pair(pins.get(i), pins.get((i + minPinDistance + 1 + j) % nPins));
				pairs.putIfAbsent(pair, bresenham(pair));
			}
		}
		
		return pairs;
	}
	
	private static ArrayList<Point> bresenham(Pair pair) {
		
		Point a = pair.getA().getPosition();
		Point b = pair.getB().getPosition();
		
		int dx, dy,
				incX, incY,
				err, err2;
		
		dx = Math.abs(b.x - a.x);
		dy = -Math.abs(b.y - a.y);
		
		incX = a.x < b.x ? 1 : -1;
		incY = a.y < b.y ? 1 : -1;
		
		err = dx + dy;
		
		ArrayList<Point> line = new ArrayList<>();
		Point currentPixel = new Point(a.x, a.y);
		
		while (true) {
			line.add(new Point(currentPixel));
			
			if (currentPixel.x == b.x && currentPixel.y == b.y) {
				break;
			}
			
			err2 = 2 * err;
			
			if (err2 > dy) {
				err += dy;
				currentPixel.x += incX;
			}
			
			if (err2 < dx) {
				err += dx;
				currentPixel.y += incY;
			}
		}
		
		return line;
	}
	
	public static int calculateYarnLength(int imageWidth, ArrayList<Pin> steps, int diameter) {
		
		double lenght = 0;
		
		for (int i = 0; i < steps.size() - 1; i++) {
			Pin a = steps.get(i);
			Pin b = steps.get(i + 1);
			
			int x = a.getPosition().x - b.getPosition().x;
			int y = a.getPosition().y - b.getPosition().y;
			lenght += Math.sqrt(x * x + y * y);
		}
		lenght = lenght / imageWidth * diameter;
		
		return (int)lenght;
	}
	
	public static ArrayList<Pin> calculatePattern(ArrayList<Pin> pins,
	                                                         int nStrings, int nPins, int fadeValue,
	                                                         HashMap<Pair, ArrayList<Point>> pairs,
	                                                         Image img2) {
		
		ArrayList<Pin> steps = new ArrayList<>();
		
//		Pin currentPin = pins.get(ThreadLocalRandom.current().nextInt(0, nPins + 1));
		Pin currentPin = pins.get(0);
		
		steps.add(currentPin);
		
		for (int i = 0; i < nStrings; i++) {
			System.out.println("loop n°" + i);
			
			Pair bestPair = null;
			int maxScore = 0;
			
			for (Pair pair : pairs.keySet()) {
				if (pair.contains(currentPin)) {
					int score = 0;
					int count = 0;
					for (Point point : pairs.get(pair)) {
						int c = img2.get(point.x, point.y) & 0xff;
						score += 0xff - c;
						count++;
					}
					//TODO add option whether to use absolute or average line score
					score = score / count;
					if (score > maxScore) {
						maxScore = score;
						bestPair = pair;
					}
				}
			}
			
			Pin nextPin;
			
			if (bestPair == null) {
				System.out.println("bestpair null. pairs left: " + pairs.size());
				break;
				
				//TODO continue with random pin?
				//no pairs left for currentpin
				//continue with random pin
//					nextPin = pins.get((round(random(nPins - 1))));
//					continue;
			} else {
				
				nextPin = bestPair.getOther(currentPin);
				
				//TODO add option whether to reuse pin pairs or not
				for (Point point : pairs.remove(bestPair)) {
					              				
					int c = img2.get(point.x, point.y) & 0xff;
					c += fadeValue;
					if (c > 0xff) {
						c = 0xff;
					}
					
					int a = 0xff000000;
					int r = c << 16;
					int g = c << 8;
					int b = c;
					c = a + r + g + b;
					
					img2.set(point.x, point.y, c);
				}
			}
			steps.add(nextPin);
			currentPin = nextPin;
		}
		return steps;
	}
}

import lu.buttel.yarnimager.util.Pair;
import lu.buttel.yarnimager.util.Pin;
import lu.buttel.yarnimager.util.Point;
import lu.buttel.yarnimager.Gui;
import lu.buttel.yarnimager.util.YarnImager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap; 

class YarnImagerTest {
	
	private final Gui tester = new Gui();
	
	@Test
	void testPinGeneration() {
		
		int radius = 640;
		int nPins = 200;
		
		ArrayList<Pin> pins = YarnImager.calculatePinPositions(radius, radius, radius, nPins);
		assertEquals(nPins , pins.size());
		
		nPins = 28;
		pins = YarnImager.calculatePinPositions(radius, radius, radius, nPins);
		assertEquals(nPins, pins.size());
	}
	
	@Test
	void testEqualPairs() {
		
		Pair x = new Pair(new Pin(0, 0, 0), new Pin(1, 1, 1));
		Pair y = new Pair(new Pin(1, 1, 1), new Pin(0, 0, 0));
		
		Assertions.assertEquals(x, y);
		assertTrue(x.hashCode() == y.hashCode());
		
		Pair z = new Pair(new Pin(0, 1, 2), new Pin(0, 0, 0));
		assertNotEquals(x, z);
		assertFalse(x.hashCode() == z.hashCode());
	}
	
	@Test
	void testHashmap() {
		
		HashMap<Pair, ArrayList<Point>> pairs = new HashMap<>();
		
		Pin a = new Pin(0, 0, 0);
		Pin b = new Pin(1, 0, 1);
		Pin c = new Pin(0, 1, 2);
		
		pairs.putIfAbsent(new Pair(a, b), new ArrayList<>());
		pairs.putIfAbsent(new Pair(a, c), new ArrayList<>());
		pairs.putIfAbsent(new Pair(b, c), new ArrayList<>());
		
		Pin d = new Pin(0, 0, 0);
		Pin e = new Pin(1, 0, 1);
		Pair de = new Pair(e, d);
			
		pairs.putIfAbsent(de, new ArrayList<>());
		
		assertTrue(pairs.size() == 3);
	}
	
	@Test
	void testPairGeneration() {
		
		int radius = 640;
		int nPins = 200;
		int minDistance = 10;
		
		ArrayList<Pin> pins = YarnImager.calculatePinPositions(radius, radius, radius, nPins);
		HashMap<Pair, ArrayList<Point>> pairs = YarnImager.calculatePinPairsAndLines(pins, minDistance);
		assertEquals(nPins * (nPins - 1 - 2 * minDistance) / 2, pairs.size());
		
		
		nPins = 28;
		pins = YarnImager.calculatePinPositions(radius, radius, radius, nPins);
		pairs = YarnImager.calculatePinPairsAndLines(pins, minDistance);
		assertEquals(nPins * (nPins - 1 - 2 * minDistance) / 2, pairs.size());
		
	}
}
